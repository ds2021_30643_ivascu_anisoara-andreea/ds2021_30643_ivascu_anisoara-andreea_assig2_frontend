import React from 'react';

import BackgroundImg from '../commons/images/devices.png';

import {Button, Container, Input, Jumbotron} from 'reactstrap';
import validate from "../home/components/validators/user-validators";
import * as API_LOGIN from "../home/api/login-api";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {withRouter} from "react-router-dom";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "720px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = {
            role: '',
            errorStatus: 0,
            error: null,

            formIsValid: false,

            username: {
                value: '',
                placeholder: 'username...',
                valid: false,
                touched: false,
                validationRules: {
                    minLength: 5,
                    isRequired: true
                }
            },

            password: {
                value: '',
                placeholder: 'password',
                valid: false,
                touched: false,
                validationRules: {
                    emailValidator: true
                }
            },


            userId: 0

        };

        this.handleChange = this.handleChange.bind(this);

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;


        const rules = this.state.username.validationRules;
        if (name === "username") {
            this.setState(
                {
                    username:
                        {
                            valid: validate(value, rules),
                            value: value


                        }
                }
            )
        } else {
            this.setState({

                password: {
                    valid: validate(value, rules),
                    value: value

                }
            });
        }
    };

    registerLogin(user){
        return API_LOGIN.login(user, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    role: result.role });
                localStorage.setItem("role",result.role);
                localStorage.setItem("userId",result.userId);
                user.username = result.username;
                user.role = result.role;
                if (user.role==="admin")
                    //console.log("patient ar trebui sa fuga");
                    this.props.history.push("/client" );

                else
                {


                }
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }



    handleSubmit(e) {

        e.preventDefault();
        console.log("User log in:");
        console.log("Username: " + this.state.username.value);

        let user = {
            username: this.state.username.value,
            password: this.state.password.value

        };
        this.registerLogin(user);
    }


    render() {

        return (


            <form className={'Home'}>
                <Jumbotron fluid style={backgroundStyle}>
                    <h1 style={{ color: 'red' }}className={'LoginTitle'}>Log In</h1>



                <p style={{ color: 'red' }}> Username: </p>

                <Input  placeholder="Hello there" style={{width: "320px"}} name="username"
                       placeholder={this.state.username.placeholder}
                       value={this.state.username.value}
                       onChange={this.handleChange}
                       touched={this.state.username.touched}
                       valid={this.state.username.valid}
                />
                {this.state.username.touched && !this.state.username.valid &&
                <div className={"error-message row"}> * username must have at least 5 characters </div>}

                <p style={{ color: 'red' }}> Password: </p>
                <Input  placeholder="Hello there" style={{width: "320px"}} name="password"
                       type={"password"}
                       placeholder={this.state.password.placeholder}
                       value={this.state.password.value}
                       onChange={this.handleChange}
                       touched={this.state.password.touched}
                       valid={this.state.password.valid}
                />
                {this.state.password.touched && !this.state.password.valid &&
                <div className={"error-message"}> * Password must have a valid format</div>}



                <Button
                        type={"submit"}
                        onClick={(e) => this.handleSubmit(e)}
                        >
                    Log in
                </Button>


                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}





                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>This assignment represents the first module of the distributed software system "Integrated
                            Medical Monitoring Platform for Home-care assistance that represents the final project
                            for the Distributed Systems course. </b> </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn
                                More</Button>
                        </p>
                    </Container>
                </Jumbotron>


            </form>
        )
    };
}


export default withRouter(Home);