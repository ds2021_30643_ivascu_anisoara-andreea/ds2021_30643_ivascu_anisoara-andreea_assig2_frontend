import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/sensor-api"

import SensorForm from "./components/sensor-form";
import SensorTable from "./components/sensor-table";


import SensorFormUpdate from "./components/sensor-form-update";




const updateStyle ={
    marginLeft:"20px"
}

class SensorContainer extends React.Component {

    constructor(props) {
        super(props);

        this.toggleFormSensor = this.toggleFormSensor.bind(this);


        this.updateToggleFormSensor = this.updateToggleFormSensor.bind(this);
        this.reloadSensor = this.reloadSensor.bind(this);
        this.updateReloadSensor = this.updateReloadSensor.bind(this);
        this.reloadAfterDelete = this.reloadAfterDelete.bind(this);
        this.deleteSensor = this.deleteSensor.bind(this);
        this.updateSensor = this.updateSensor.bind(this);

        this.state = {


            updatedSensor:false,
            sensorData: null,
            selectedSensor: false,
            collapseFormSensor: false,
            tableDataSensor: [],
            isLoadedSensor: false,
            errorStatusSensor: 0,
            errorSensor: null,


        };
    }

    componentDidMount() {
        this.fetchSensors();

    }



    deleteSensor(sensor){
        API_USERS.deleteSensor(sensor,(result, status, err) => this.reloadAfterDelete());
    }



    updateSensor(sensor) {
        this.state.sensorData = sensor;
        this.updateToggleFormSensor();
    }



    fetchSensors() {
        return API_USERS.getSensors((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableDataSensor: result,
                    isLoadedSensor: true
                });
            } else {
                this.setState(({
                    errorStatusSensor: status,
                    errorSensor: err
                }));
            }
        });
    }




    toggleFormSensor() {
        this.setState({selectedSensor: !this.state.selectedSensor});
    }

    updateToggleFormSensor(){
        this.setState({updatedSensor: !this.state.updatedSensor});
    }


    reloadSensor(){
        this.setState({
            isLoadedSensor:false
        });
        this.toggleFormSensor();

        this.fetchSensors();

    }


    updateReloadSensor() {
        this.setState({
            isLoadedSensor: false
        });

        this.updateToggleFormSensor();
        this.fetchSensors();

    }

    reloadAfterDelete() {
        this.setState({
            isLoadedSensor: false
        });
        this.fetchSensors();
    }



    render() {

        return (
            <div>

                <CardHeader>
                    <strong> Sensor Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleFormSensor}>Add Sensor </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoadedSensor && <SensorTable tableData = {this.state.tableDataSensor} update={this.updateSensor} delete={this.deleteSensor}/>}
                            {this.state.errorStatusSensor > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatusSensor}
                                error={this.state.errorSensor}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedSensor} toggle={this.toggleFormSensor}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormSensor}> Add Sensor: </ModalHeader>
                    <ModalBody>
                        <SensorForm reloadHandler={this.reloadSensor}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.updatedSensor} toggle={this.updateToggleFormSensor}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.updateToggleFormSensor}> Update </ModalHeader>
                    <ModalBody>
                        <SensorFormUpdate reloadHandler={this.updateReloadSensor} selectedSensor = {this.state.sensorData}/>
                    </ModalBody>
                </Modal>


            </div>


        )

    }
}


export default SensorContainer;
