import React from "react";
import Table from "../../commons/tables/table";
import Button from "react-bootstrap/Button";

const filters = [
    {
        accessor: 'description'

    },
    {
        accessor: 'address'

    }

];


class DeviceTable extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };



        this.columns = [

            {
                Header: 'Description',
                accessor: 'description',
            },
            {
                Header: 'Address',
                accessor: 'address',
            },
            {
                Header: 'MaximumEnergyConsumption',
                accessor: 'maximumEnergyConsumption',
            },
            {
                Header: 'AverageEnergyConsumption',
                accessor: 'averageEnergyConsumption',
            },


            {
                Header: 'Delete',
                Cell: row =>(<Button color="primary" onClick={() => this.props.delete(row.original)}>Delete </Button>)
            },
            {
                Header: 'Update',
                Cell: row =>(<Button color="primary" onClick={() => this.props.update(row.original)}>Update </Button>)
            }

        ];


    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}


export default DeviceTable;