import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    device: '/device'
};

function getDevices(callback) {
    let request = new Request(HOST.backend_api + endpoint.device, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getDeviceById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.device + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postDevice(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

/*function deleteDevice(deviceId, callback){
    let request = new Request(HOST.backend_api + endpoint.device + "/" + deviceId , {
        method: 'DELETE'
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}*/

function deleteDevice(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.device + "/delete/" + params.id, {
        method: 'POST'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

/*function updateDevice(user,id, callback){
    console.log(user.id);
    let request = new Request(HOST.backend_api + endpoint.device + "/" + id , {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}*/

function updateDevice(user, callback){
    let request = new Request(HOST.backend_api + endpoint.device +"/update" ,{
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getDevices,
    getDeviceById,
    postDevice,
    deleteDevice,
    updateDevice
};
