import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import * as API_USERS from "./api/device-api"

import DeviceForm from "./components/device-form";
import DeviceTable from "./components/device-table";


import DeviceFormUpdate from "./components/device-form-update";




const updateStyle ={
    marginLeft:"20px"
}

class DeviceContainer extends React.Component {

    constructor(props) {
        super(props);

        this.toggleFormDevice = this.toggleFormDevice.bind(this);


        this.updateToggleFormDevice = this.updateToggleFormDevice.bind(this);
        this.reloadDevice = this.reloadDevice.bind(this);
        this.updateReloadDevice = this.updateReloadDevice.bind(this);
        this.reloadAfterDelete = this.reloadAfterDelete.bind(this);
        this.deleteDevice = this.deleteDevice.bind(this);
        this.updateDevice = this.updateDevice.bind(this);

        this.state = {


            updatedDevice:false,
            deviceData: null,
            selectedDevice: false,
            collapseFormDevice: false,
            tableDataDevice: [],
            isLoadedDevice: false,
            errorStatusDevice: 0,
            errorDevice: null,


        };
    }

    componentDidMount() {
        this.fetchDevices();

    }



    deleteDevice(device){
        API_USERS.deleteDevice(device,(result, status, err) => this.reloadAfterDelete());
    }



    updateDevice(device) {
        this.state.deviceData = device;
        this.updateToggleFormDevice();
    }



    fetchDevices() {
        return API_USERS.getDevices((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableDataDevice: result,
                    isLoadedDevice: true
                });
            } else {
                this.setState(({
                    errorStatusDevice: status,
                    errorDevice: err
                }));
            }
        });
    }




    toggleFormDevice() {
        this.setState({selectedDevice: !this.state.selectedDevice});
    }

    updateToggleFormDevice(){
        this.setState({updatedDevice: !this.state.updatedDevice});
    }


    reloadDevice(){
        this.setState({
            isLoadedDevice:false
        });
        this.toggleFormDevice();

        this.fetchDevices();

    }


    updateReloadDevice() {
        this.setState({
            isLoadedDevice: false
        });

        this.updateToggleFormDevice();
        this.fetchDevices();

    }

    reloadAfterDelete() {
        this.setState({
            isLoadedDevice: false
        });
        this.fetchDevices();
    }



    render() {

        return (
            <div>

                <CardHeader>
                    <strong> Device Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleFormDevice}>Add Device </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoadedDevice && <DeviceTable tableData = {this.state.tableDataDevice} update={this.updateDevice} delete={this.deleteDevice}/>}
                            {this.state.errorStatusDevice > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatusDevice}
                                error={this.state.errorDevice}
                            />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedDevice} toggle={this.toggleFormDevice}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleFormDevice}> Add Device: </ModalHeader>
                    <ModalBody>
                        <DeviceForm reloadHandler={this.reloadDevice}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.updatedDevice} toggle={this.updateToggleFormDevice}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.updateToggleFormDevice}> Update </ModalHeader>
                    <ModalBody>
                        <DeviceFormUpdate reloadHandler={this.updateReloadDevice} selectedDevice = {this.state.deviceData}/>
                    </ModalBody>
                </Modal>


            </div>


        )

    }
}


export default DeviceContainer;
